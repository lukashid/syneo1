﻿using System;

namespace KolektorApp.Models.Entities
{
    /// <summary>
    /// The word model
    /// </summary>
    public class Word
    {
        public int WordId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

        public Word()
        {
            Quantity = 1;
        }
    }
}
