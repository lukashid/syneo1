﻿using System;
using System.Data.Entity;
using KolektorApp.Models.Entities;

namespace KolektorApp.Models
{
    /// <summary>
    /// The person db context.
    /// </summary>
    public class WordDbContext : DbContext
    {
        public WordDbContext() : base("name=WordContext")
        {
            Database.SetInitializer<WordDbContext>(new CreateDatabaseIfNotExists<WordDbContext>());
        }
        public DbSet<Word> Words { get; set; }
    }
}
