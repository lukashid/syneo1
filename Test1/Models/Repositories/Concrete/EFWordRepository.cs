﻿using System;
using System.Linq;
using KolektorApp.Models.Entities;
using KolektorApp.Models.Repositories.Abstract;

namespace KolektorApp.Models.Repositories.Concrete
{
    /// <summary>
    /// Word repository based on entity framework
    /// </summary>
    public class EFWordRepository : IWordRepository
    {
        private WordDbContext _dbContext;

        public EFWordRepository(WordDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Word> Words
        {
            get
            {
                return _dbContext.Words;
            }
        }

        /// <summary>
        /// Get a quantity for the word
        /// </summary>
        /// <param name="name">The word name</param>
        public int GetQuantity(string name)
        {
            return  _dbContext.Words.Where(w => (w.Name == name))
                                     .FirstOrDefault().Quantity;
        }

        /// <summary>
        /// Add a new word
        /// </summary>
        /// <param name="name">The word name</param>
        public void AddWord(string name)
        {
            bool exist = _dbContext.Words.Any(w => (w.Name == name));

            if(exist)
            {
                Word word = Words.Where(w => (w.Name == name))
                    .FirstOrDefault();
                word.Quantity++;
            }
            else
            {
                Word word = new Word { Name = name };
                _dbContext.Words.Add(word);
            }
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Removes all words from database
        /// </summary>
        public void EraseData()
        {
            var allWords = _dbContext.Words.Select(w => w);
            _dbContext.Words.RemoveRange(allWords);
            _dbContext.SaveChanges();
        }
    }
}
