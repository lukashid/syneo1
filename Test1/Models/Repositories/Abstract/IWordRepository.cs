﻿using System;
using KolektorApp.Models.Entities;
using System.Linq;

namespace KolektorApp.Models.Repositories.Abstract
{
    public interface IWordRepository
    {
        IQueryable<Word> Words { get; }
        int GetQuantity(string name);
        void AddWord(string name);
        void EraseData();
    }
}
