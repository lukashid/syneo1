﻿using System;
using KolektorApp.Models;
using KolektorApp.Controllers;
using KolektorApp.Models.Repositories.Concrete;
using KolektorApp.Utility;

namespace KolektorApp
{
    public class Kolektor
    {
        static void Main(string[] args)
        {
            EFWordRepository wordRepository = new EFWordRepository(new WordDbContext());
            CLIController cliController = new CLIController(wordRepository);
            InputHandler inputHandler = new InputHandler(args);
            
            if(inputHandler["p"] != null && inputHandler["p"] != "true")
            {
                cliController.AddWord(inputHandler["p"]);
            }
            else if(inputHandler["p"] != null && inputHandler["p"] == "true")
            {
                Console.Write("Please insert the word: ");
                string word = Console.ReadLine();
                cliController.AddWord(word);
            }

            if (inputHandler["s"] != null)
            {
                cliController.PrintQuantity(inputHandler["s"]);
            }

            if (inputHandler["e"] != null)
            {
                cliController.EraseData();
            }
        }

    }
}
