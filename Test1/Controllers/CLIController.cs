﻿using System;
using KolektorApp.Models.Repositories.Abstract;

namespace KolektorApp.Controllers
{
    /// <summary>
    /// CLI Application controller realizes
    /// required functionality
    /// </summary>
    public class CLIController
    {
        IWordRepository _wordRepository;
        public CLIController(IWordRepository wordRepository)
        {
            _wordRepository = wordRepository;
        }

        /// <summary>
        /// Adds the word to database
        /// </summary>
        /// <param name="name">The word name</param>
        public void AddWord(string name)
        {
            try
            {
                _wordRepository.AddWord(name);
            }
            catch(Exception e)
            {
                Console.WriteLine("Some issues has been noticed!");
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Word '" + name + "' has been added.");                        
        }

        /// <summary>
        /// Prints the word in console
        /// </summary>
        /// <param name="name">The word name</param>
        public void PrintQuantity(string name)
        {
            try
            {
                int quantity = _wordRepository.GetQuantity(name);
                Console.WriteLine("Total quantity for '" + name + "' word : " + quantity);
            }
            catch(Exception e)
            {
                Console.WriteLine("Some issues has been noticed!");
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Removes all words from database
        /// </summary>
        /// <param name="name">The word name</param>
        public void EraseData()
        {
            try
            {
                _wordRepository.EraseData();
            }
            catch(Exception e)
            {
                Console.WriteLine("Some issues has been noticed!");
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("All data has been erased!");

        }
    }
}
