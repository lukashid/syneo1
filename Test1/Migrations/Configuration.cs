namespace KolektorApp.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<KolektorApp.Models.WordDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "KolektorApp.Models.WordDbContext";
        }

        protected override void Seed(KolektorApp.Models.WordDbContext context)
        {
            //  This method will be called after migrating to the latest version.
        }
    }
}
