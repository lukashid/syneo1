namespace KolektorApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AutoMigrations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Words", "Quantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Words", "Quantity");
        }
    }
}
