﻿using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace KolektorApp.Utility
{
    public class InputHandler
    {
        private StringDictionary _params;

        public InputHandler(string[] args)
        {
            _params = new StringDictionary();
            Regex spliter = new Regex(@"^-{1,2}",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

            Regex remover = new Regex(@"^['""]?(.*?)['""]?$",
                RegexOptions.IgnoreCase | RegexOptions.Compiled);

            string param = null;
            string[] splits;

            // Valid parameters forms
            foreach (string Txt in args)
            {
                // Look for new parameters
                splits = spliter.Split(Txt, 3);

                switch (splits.Length)
                {
                    // Found a value
                    case 1:
                        if (param != null)
                        {
                            if (!_params.ContainsKey(param))
                            {
                                splits[0] =
                                    remover.Replace(splits[0], "$1");

                                _params.Add(param, splits[0]);
                            }
                            param = null;
                        }
                        // No parameter waiting for a value
                        break;

                    // Found a parameter
                    case 2:
                        // The last parameter is still waiting.
                        if (param != null)
                        {
                            if (!_params.ContainsKey(param))
                                _params.Add(param, "true");
                        }
                        param = splits[1];
                        break;

                    // Parameter with enclosed value
                    case 3:
                        // The last parameter is still waiting. 
                        if (param != null)
                        {
                            if (!_params.ContainsKey(param))
                                _params.Add(param, "true");
                        }

                        param = splits[1];

                        // Remove possible enclosing characters (",')
                        if (!_params.ContainsKey(param))
                        {
                            splits[2] = remover.Replace(splits[2], "$1");
                            _params.Add(param, splits[2]);
                        }

                        param = null;
                        break;
                }
            }

            // In case a parameter is still waiting
            if (param != null)
            {
                if (!_params.ContainsKey(param))
                    _params.Add(param, "true");
            }
        }

        // Retrieve a parameter value if it exists 
        // (overriding C# indexer property)
        public string this[string param]
        {
            get
            {
                return (_params[param]);
            }
        }
    }
}
